package com.example.davalebanomeri3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.example.davalebanomeri3.adapter.adapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager2: ViewPager2
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tabLayout = findViewById(R.id.TbView)
        viewPager2 = findViewById(R.id.ViewPager)

        viewPager2.adapter = adapter(this)
        TabLayoutMediator(tabLayout,viewPager2){
            tab,position ->
            tab.text = "position ${position + 1}"
        }.attach()
    }
}