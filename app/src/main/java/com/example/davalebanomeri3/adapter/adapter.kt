package com.example.davalebanomeri3.adapter

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.davalebanomeri3.fragments.FragmentFirst
import com.example.davalebanomeri3.fragments.FragmentSecond
import com.example.davalebanomeri3.fragments.FragmentThird

class adapter(activity : AppCompatActivity) : FragmentStateAdapter (activity) {
    override fun getItemCount() = 3

    override fun createFragment(position: Int): Fragment {
        if (position == 0) {
            return FragmentFirst()
        } else {
            if (position == 1) {
                return FragmentSecond()
            }
        }
        return FragmentThird()

    }

}